$(document).ready(function(){
    $('.menu ul li ul').ready(function(){
        $('.menu ul li ul:eq(0)')
            .parent()
            .parent()
            .parent()
            .css('margin-bottom', $('.menu ul li ul:eq(0)').height() + 60);
    });

    $('#login-button').click(function(){
        $('#login-box').show();
        $('#login-box .close').click(function(){
            $('#login-box').hide();
        });
    });

    $('.tabs ul li').click(function(){
        var $tabList = $(this)
                        .parents('.tabs');

        $tabList
            .next()
            .find('.tab-content')
            .hide();

        $tabList.find('li').removeClass('active');
        $(this).addClass('active');

        $( $(this).find('a').attr('href') ).show();
        return false;
    })

    $('.tabs ul li:first-child').click();


    /**
     * Галерейка
     */
    $('.item-gallery').ready(function(){
        // Переменная с той хуйней, которая дрыгаться будет
        var $movedUl = $('.item-gallery__inner ul');

        // Переменная с баром
        var $bar = $('.scroller');

        // Сперва проставляем правильные размеры
        var shift = ( ($(document).width() - $('.container').width()) / 2 )

        $('.item-gallery__inner').css({
            'width': $(document).width(),
            'margin-left': - shift
        })

        // Теперь считаем ширину
        var ulWidth = 0;
        $movedUl.find('li').each(function(i, obj){
            // Тут хак для holder.js
            var widthHolderImg = $(obj)
                    .find('img')[0]
                    .src
                    .replace(/(http.+\/)/, '')
                    .replace(/x\d+/, '');

            // Если это holder.js то подставляем ширину через жопу
            if ( widthHolderImg != undefined && widthHolderImg != 0) {
                $(obj).width( widthHolderImg );
            } else {
                $(obj).width( $(obj).find('img').width() );
            }
            ulWidth += $(obj).outerWidth() + 15
        })

        // Соотношение между шириной блока с картинками и шириной бара
        var proportion = ulWidth / $bar.width();

        // Выставляем ul-шичке правильную ширину и отступ с пресетом
        $movedUl.css({
            'width': ulWidth,
            'margin-left': shift
        });


        // Гребанная лапшка-оторви-мне-ноги. Тут я через жопу и криво пишу скроллер.
        // Инета нет, даже подсмотреть негде.
        var bulletDown = false;
        var $bullet = $('.scroller .bullet');

        var moved = function( pageX, cursorPosition ) {
            var leftPosition = $bar.offset().left;
            if ( cursorPosition === undefined ) {
                cursorPosition = pageX - leftPosition;
            }

            // Если не выходим за границы бара, то двигаем
            if ( cursorPosition < 0 || cursorPosition > $bar.width() ) {
                return;
            } else {
                $bullet.css('left', cursorPosition);

                $movedUl.css('left', - (
                        ( $movedUl.width() - $('.container').width() - shift ) / (
                            $bar.width() / parseInt(cursorPosition)
                        )
                    )
                );
            }
        }

        // Навешиваем собитие скролла
        $(document).bind('mousemove.noUiSlider', function(ev){
            // Если стоит флаг, что мы нажали на пульку, то действуем!
            if ( bulletDown ) {
                moved( ev.pageX );
            }
        });

        // Если юзер "отжал" кнопку мыши, то бросаем это гиблое дело
        $(document).bind('mouseup.noUiSlider', function(){
            bulletDown = false;
        });

        // А если нажал, то в путь!
        $bullet.mousedown(function(){
            bulletDown = true;
        });

        $movedUl.mwheelIntent(function(ev, speed){
            var delta = speed > 0 ? 10 : -10;
            console.log(delta);
            moved( 0, parseInt( $bullet.css('left') ) + delta );
            return false;
        })

        // А тут обрабатываем переход по клику на баре
        $bar.click(function(ev){
            moved( ev.pageX );
        })
    });

    /**
     * Календарь мероприятий
     */
    $('#calendar-link').click(function(){
        $('#calendar').show();
        return false;
    });
    $('#calendar-close').click(function(){
        $('#calendar').hide();
        return false;
    });

    /**
     * Коллапсирующая шапка
     */
	$(window).scroll( function() {
        var value = $(this).scrollTop();
        if ( value > 200 ) {
            $("#header, .wrap, body").addClass('micro');
        } else {
            $("#header, .wrap, body").removeClass('micro');
        }
    });


    /**
     * Каруселька на главной
     */
    var setVisibilityControls = function( $caorusel ) {
        var $next = $('.repertoire .next');
        var $prev = $('.repertoire .prev');

        if ( !$caorusel.jcarousel('hasPrev') ) {
            $prev.addClass('passive');
        } else {
            $prev.removeClass('passive');
        }
        if ( !$caorusel.jcarousel('hasNext') ) {
            $next.addClass('passive');
        } else {
            $next.removeClass('passive');
        }
    }
    var $caorusel = $('.repertoire .repertoire__inner').jcarousel({

    });

    $('.repertoire .prev').click(function() {
        $caorusel.jcarousel('scroll', '-=3');
        setVisibilityControls($caorusel);
        return false;
    });

    $('.repertoire .next').click(function() {
        $caorusel.jcarousel('scroll', '+=3');
        setVisibilityControls($caorusel);
        return false;
    });

    $('[data-toggle="tooltip"]').tooltip();
});